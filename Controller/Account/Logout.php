<?php

namespace Swiss\Ajax\Controller\Account;

use Magento\Customer\Controller\AbstractAccount;
use Magento\Customer\Model\AccountManagement;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Webapi\Exception as ApiException;
use Swiss\Ajax\Block\Logout as LogoutBlock;

class Logout extends AbstractAccount
{

    /**
     * @var string Contains the path to the login-template.
     */
    const LOGOUT_TEMPLATE = 'Swiss_Ajax::account/response.phtml';

    /**
     * @var string Contains the path to the login-block.
     */
    const LOGOUT_BLOCK = LogoutBlock::class;

    /**
     * @var Magento\Framework\View\Result\PageFactory
     */
    private $_resultPageFactory;

    /**
     * @var mixed
     */
    private $_resultJsonFactory;

    /**
     * @var mixed
     */
    protected $_objectManager;

    /**
     * @var mixed
     */
    private $_session;

    /**
     * @var mixed
     */
    private $_customerAccountManagement;

    /**
     * @param Context     $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        AccountManagement $accountManagement
    ) {
        $this->_resultPageFactory         = $resultPageFactory;
        $this->_resultJsonFactory         = $resultJsonFactory;
        $this->_objectManager             = ObjectManager::getInstance();
        $this->_customerAccountManagement = $accountManagement;

        $this->_session = $this->_objectManager->create(Session::class);

        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        $result     = $this->_resultJsonFactory->create();
        $resultPage = $this->_resultPageFactory->create();
        $data       = ['success' => false];

        if ($this->getRequest()->isPost() && $this->_session->isLoggedIn()) {
            try {
                $this->_session->logout();
                $data['success'] = true;
            } catch (Exception $e) {
                error_log($e->getMessage() . "\n", 3, '/tmp/swiss-err.log');
                $result->setHttpResponseCode(
                    ApiException::HTTP_INTERNAL_ERROR
                );
            }
        }

        $block = $resultPage->getLayout()
            ->createBlock(self::LOGOUT_BLOCK)
            ->setTemplate(self::LOGOUT_TEMPLATE)
            ->setData('data', $data)
            ->toHtml();

        $result->setData($block);
        return $result;
    }
}
